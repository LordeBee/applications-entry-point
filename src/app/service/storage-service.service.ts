import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageServiceService {

  constructor() { }

  saveInfo(key:string, info:string) {
    localStorage.setItem(key, info);
  }

  getInfo(key: string) {
    return localStorage.getItem(key);
  }

  clearInfo(key:string) {
    localStorage.removeItem(key);
  }

  loggedIn(){
    return localStorage.getItem("appsData");
  }

}
