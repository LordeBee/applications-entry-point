import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {

  private baseUrl = "https://ghuatgodigisrv1.gh.sbicdirectory.com:8005/moby/api/";
  private loginURL = this.baseUrl + "userLogin";
 


  
  private httpHeaders = new HttpHeaders()
    .set('Content-Type', 'application/json')
    .set('Access-Control-Allow-Origin', '*')
    .set("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT")
    .set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization")
    .set("sourceCode", "AuthServices")
    .set("countryCode", "GH")

  private options = {
    headers: this.httpHeaders
  };


  constructor(private _http:HttpClient) { }

  authUser(data){
    return this._http.post(this.loginURL, data, this.options);
  }


}
