import { Injectable } from '@angular/core';
import { CanActivate, Router} from '@angular/router';
import { StorageServiceService } from '../service/storage-service.service';



@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private storage_service: StorageServiceService, private router: Router){
   
  }

  canActivate(): boolean{
    if(this.storage_service.loggedIn()){
      return true;
    }

    else{
      this.router.navigate(['/login']);
      return false;
    }

  }
  
}
