import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { toast } from 'angular2-materialize';
import { ApiServiceService } from '../service/api-service.service';
import { StorageServiceService } from '../service/storage-service.service';
import { loginModel } from '../models/loginModel';
import { hostHeaderInfo } from '../models/hostHeaderInfo';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  resp: any;
  loading=false;

  public loginModel : loginModel;
  public hostHeaderInfo : hostHeaderInfo;

  constructor(private api_service: ApiServiceService, private router: Router, private storage_service: StorageServiceService) { 
    this.loginModel = new loginModel();
    this.hostHeaderInfo = new hostHeaderInfo();
    this.loginModel.hostHeaderInfo = this.hostHeaderInfo;
  
  }

  ngOnInit() {

  }

  login(){
    if(this.loginModel.username === ''){
      this.showToast('Please enter your SAP number');
    }else if (this.loginModel.password === ''){
      this.showToast('Please enter your password')
    }else{
      this.loading = true;
      this.loginModel.username = this.loginModel.username.toUpperCase();
      var data = JSON.stringify(this.loginModel);
      // alert(data);
      this.api_service.authUser(data).subscribe(res => {
        this.resp = res;
        this.loading = false;
        if(this.resp.hostHeaderInfo.responseCode == "000"){
          
          this.storage_service.saveInfo('userAuth', "true");
          this.storage_service.saveInfo('appsData', JSON.stringify(this.resp));
          this.router.navigate(['/apps']);

          
        }else{
          toast(this.resp.hostHeaderInfo.responseMessage.toLowerCase(), 3000);
        }
    }, error => {
        toast('Oops. Please try again later', 3000);
        this.loading = false;
    });
    }
  }

  showToast(message: string) {
    toast(message, 3000);
  }

}
