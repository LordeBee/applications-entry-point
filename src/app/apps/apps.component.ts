import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { toast } from 'angular2-materialize';
import { ApiServiceService } from '../service/api-service.service';
import { StorageServiceService } from '../service/storage-service.service';
import { userObj } from '../models/userObj';

@Component({
  selector: 'app-apps',
  templateUrl: './apps.component.html',
  styleUrls: ['./apps.component.css']
})
export class AppsComponent implements OnInit {

  appsData:any;
  username:any;
  apps:any;
  count:any;
  public userObj : userObj;
  
  constructor(private api_service: ApiServiceService, private router: Router, private storage_service: StorageServiceService) { 

    this.userObj = new userObj();

   }

  ngOnInit() {

    this.appsData = JSON.parse(this.storage_service.getInfo('appsData'));
    this.username = this.appsData.staffInfo.firstName;
    this.apps = this.appsData.role;
    this.count = this.appsData.role.length;
    console.log(this.appsData);

  }


  logout(){
    this.storage_service.clearInfo('appsData');
    this.router.navigate(['/login']);
  }

  gotoApp(app,url,role){

    if(app=="COLLATERAL"){
      this.userObj.fullname = this.appsData.staffInfo.firstName + " " + this.appsData.staffInfo.lastName;
      this.userObj.username = this.appsData.staffInfo.staffNo;
      this.storage_service.saveInfo('collateralUserObj', JSON.stringify(this.userObj));
      console.log(this.storage_service.getInfo('userObj'));
       window.location.href = url;
    }
    else if(app=="MOBY CREDIT"){
      this.userObj.userID = this.appsData.staffInfo.staffNo;
      this.userObj.username = this.appsData.staffInfo.firstName.toUpperCase();
      this.userObj.role =  role;
      console.log(this.userObj);
      this.storage_service.saveInfo('creditUserObj', JSON.stringify(this.userObj));
      if(role=="CVU ADMIN"){
        window.location.href = "https://ghuatgodigisrv1.gh.sbicdirectory.com:8005/mobyCredit/cvu-admin";
      }
      else{
        window.location.href = url;
      }
     
    }
    else if(app=="SALARY PROCESSING"){
      this.userObj.userID = this.appsData.staffInfo.staffNo;
      this.userObj.username = this.appsData.staffInfo.firstName.toUpperCase();
      this.userObj.role =  role;
      console.log(this.userObj);
      this.storage_service.saveInfo('salaryUserObj', JSON.stringify(this.userObj));
      window.location.href = url;

    }else if(app=="COVENANT DIARY"){
      this.userObj.userID = this.appsData.staffInfo.staffNo;
      this.userObj.username = this.appsData.staffInfo.firstName.toUpperCase();
      this.storage_service.saveInfo('covenantUserObj', JSON.stringify(this.userObj));
      window.location.href = url;
    }
    else if(app=="MOBY FX"){
    this.userObj.fname = this.appsData.staffInfo.firstName;
    this.userObj.sname = this.appsData.staffInfo.lastName;
    this.userObj.id = this.appsData.staffInfo.staffNo;
    this.userObj.branch = this.appsData.staffInfo.department;
    this.storage_service.saveInfo('mobyfxStaff', JSON.stringify(this.userObj));
    window.location.href = url;
  
  } else if(app=="MOBY CARDS"){
    this.userObj.username = this.appsData.staffInfo.firstName;
    this.userObj.sname = this.appsData.staffInfo.lastName;
    this.userObj.userID = this.appsData.staffInfo.staffNo;
    this.userObj.role = role;
    this.storage_service.saveInfo('mobyCardsUserObj', JSON.stringify(this.userObj));
    console.log(this.userObj);
    window.location.href = url;
  }
  else if(app="MOBY LEGAL"){
    this.userObj.username = this.appsData.staffInfo.firstName;
    this.userObj.sname = this.appsData.staffInfo.lastName;
    this.userObj.userID = this.appsData.staffInfo.staffNo;
    this.userObj.role = role;
    this.storage_service.saveInfo('mobyLegalUserObj', JSON.stringify(this.userObj));
    console.log(this.userObj);
    window.location.href = url;
  }
    

  }

}
